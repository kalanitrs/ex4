import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';


@Component({
  selector: 'movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css'],
})

export class MoviesComponent implements OnInit {
  
  name = "no movie";
  displayedColumns: string[] = ['Id', 'Title', 'Studio', 'Weekend_Income', 'delete'];
  
  movies = []

  constructor(private db: AngularFireDatabase) { }

  ngOnInit() {
    this.db.list('/movies').snapshotChanges().subscribe(
      movies => {
        this.movies = [];
        movies.forEach(
          movie => {
            let y = movie.payload.toJSON();
            this.movies.push(y);
          }
        )
      }
    )
  }

  hideHim(mov) {
    let start, end = this.movies;
    let movid = mov.Id;

    if (this.movies.length == 1) {
      this.movies = [];
    }
    start = this.movies.slice(0,movid-1);
    end = this.movies.slice(movid, this.movies.length+1);
    this.movies = start.concat(end);
    this.name = mov.Title;

    for (let movie of this.movies) {
      if (movie.Id > movid) {
        movie.Id--;
      }
    }
  }
}